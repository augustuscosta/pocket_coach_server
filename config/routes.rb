Rails.application.routes.draw do

    # This line mounts Spree's routes at the root of your application.
    # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
    # If you would like to change where this engine is mounted, simply change the :at option to something different.
    #
    # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"

    devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", :sessions => "users/sessions", :registrations => 'users/registrations'}

    resources :temperaments
    resources :wheels
    resources :experiences
    resources :users, :except =>[:destroy]
    resources :tasks
    resources :goals
    resources :humors, :except =>[:destroy,:edit]
    resources :wheel_of_lives, :except =>[:destroy,:edit]
    resources :temperaments, :except =>[:destroy,:edit]
    resources :notifications

    root :to => "users#index"

    

    namespace :api do

        scope :users do
          post '/sign_in_mobile',to:'/api/sessions#create', defaults: { format: 'json' }
          post '/sign_in_mobile_facebook',to:'/api/sessions#create_facebook', defaults: { format: 'json' }
          delete '/sign_out_mobile',to:'/api/sessions#destroy'
          post '/reset_password',to:'/api/sessions#reset_password'
          post '/register_mobile_multipart',to:'/api/registrations#create_multipart', defaults: { format: 'json' }
          post '/register_mobile',to:'/api/registrations#create', defaults: { format: 'json' }
          post '/register_mobile',to:'/api/registrations#create', defaults: { format: 'json' }
        end

        scope :goals do
            patch '/sign_in_mobile',to:'/api/sessions#create', defaults: { format: 'json' }
        end

        get '/humors',to:'/api/humors#index', defaults: { format: 'json' }

        post '/create_humor',to:'/api/humors#create_humor', defaults: { format: 'json' }

        get '/goals',to:'/api/goals#index', defaults: { format: 'json' }

        post '/create_goal_image',to:'/api/goals#create_multipart_goal', defaults: { format: 'json' }

        get '/goals/:id',to:'/api/goals#show_goal', defaults: { format: 'json' }

        delete '/delete_goal/:id',to:'/api/goals#delete_goal', defaults: { format: 'json' }

        put '/finalize_goal/:id',to:'/api/goals#finalize_goal', defaults: { format: 'json' }

        get '/wheels',to:'/api/wheels#index', defaults: { format: 'json' }

        post '/new_wheel_of_life',to:'/api/wheels#create_wheel', defaults: { format: 'json' }

        get '/wheel_of_life',to:'/api/wheels#show_wheel', defaults: { format: 'json' }

        get '/temperaments',to:'/api/temperaments#index', defaults: { format: 'json' }

        post '/new_temperament',to:'/api/temperaments#create', defaults: { format: 'json' }

        get '/tasks',to:'/api/tasks#index', defaults: { format: 'json' }

        get '/tasks/:id',to:'/api/tasks#show_task', defaults: { format: 'json' }

        delete '/delete_task/:id',to:'/api/tasks#delete_task', defaults: { format: 'json' }

        put '/finalize_task/:id',to:'/api/tasks#finalize_task', defaults: { format: 'json' }

        get '/get_tasks/:goal_id',to:'/api/tasks#get_tasks', defaults: { format: 'json' }

        post '/create_task',to:'/api/tasks#create_task', defaults: { format: 'json' }

        get '/current_user',to:'/api/users#current_user_session', defaults: { format: 'json' }

        post '/device_id',to:'/api/device_ids#create', defaults: { format: 'json' }

        get '/experiences',to:'/api/experiences#index', defaults: { format: 'json' }

        put '/update_mobile_multipart',to:'/api/registrations#update_multipart', defaults: { format: 'json' }

    end

end
