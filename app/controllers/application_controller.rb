class ApplicationController < ActionController::Base
# Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  before_filter :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:date_of_birth, :avatar, :email, :password, :name, :facebook_token, :facebook_account,:gender,:child, :affective_situation, :rent, :employment_situation,
        {:humor_ids => []},{:wheels => []},{:temperaments => []}, :religion, :schooling, :experience_id)}

    devise_parameter_sanitizer.for(:account_update) {|u| u.permit(:date_of_birth, :avatar, :email, :password, :name, :facebook_token, :facebook_account,:gender,:child,:affective_situation, :rent,:employment_situation,
        {:humor_ids => []},{:wheels => []},{:temperaments => []}, :religion, :schooling, :experience_id)}
  end

  def email_required?
    false
  end

end