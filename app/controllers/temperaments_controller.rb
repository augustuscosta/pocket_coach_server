class TemperamentsController < ApplicationController
  before_action :set_temperament, only: [:show, :edit, :update, :destroy]

  # GET /temperaments
  # GET /temperaments.json
  def index
    @temperaments = Temperament.all
  end

  # GET /temperaments/1
  # GET /temperaments/1.json
  def show
  end

  # GET /temperaments/new
  def new
    @temperament = Temperament.new
  end

  # GET /temperaments/1/edit
  def edit
  end

  # POST /temperaments
  # POST /temperaments.json
  def create
    @temperament = Temperament.new(temperament_params)

    respond_to do |format|
      if @temperament.save
        format.html { redirect_to @temperament, notice: 'Temperament was successfully created.' }
        format.json { render :show, status: :created, location: @temperament }
      else
        format.html { render :new }
        format.json { render json: @temperament.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /temperaments/1
  # PATCH/PUT /temperaments/1.json
  def update
    respond_to do |format|
      if @temperament.update(temperament_params)
        format.html { redirect_to @temperament, notice: 'Temperament was successfully updated.' }
        format.json { render :show, status: :ok, location: @temperament }
      else
        format.html { render :edit }
        format.json { render json: @temperament.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /temperaments/1
  # DELETE /temperaments/1.json
  def destroy
    @temperament.destroy
    respond_to do |format|
      format.html { redirect_to temperaments_url, notice: 'Temperament was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_temperament
      @temperament = Temperament.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def temperament_params
      params.require(:temperament).permit(:will_result, :wish_result, :fury_result, :inhibition_result, :sensibility_result, :coping_result, :user_id)
    end
end
