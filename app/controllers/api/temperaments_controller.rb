class Api::TemperamentsController < Api::BaseController

	respond_to :json

	def index
		@temperaments = Temperament.where(user: current_user)
		render 'temperaments/index.json.jbuilder'
		return @temperaments
	end

	def create
		@temperament = Temperament.new(temperament_params)
		@temperament.user = current_user
		@temperament.save
		render 'temperaments/show.json.jbuilder'
	end

	def temperament_params
		params.require(:temperament).permit(:id, :will_result, :wish_result, :fury_result, :inhibition_result, :sensibility_result, :coping_result, :user_id)
	end

end