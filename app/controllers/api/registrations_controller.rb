class Api::RegistrationsController < Api::BaseController
    include Devise::Controllers

    skip_before_action :authenticate_user!


    # POST /resource
    def create
        resource = User.new(register_params)
        resource.save
        if resource.persisted?
            sign_in(resource)
            logger.info resource.errors.full_messages
            session[:user_id] = resource.id
            render json: {success: true,email: resource.email},status:200
            return
        else
            invalid_signup_attempt(resource)
        end
    end

    # POST /resource
    def create_multipart
        resource =  User.new(register_multipart_params)
        resource.experience_id = 1
        resource.account_type_id = 1
        resource.save
        if resource.persisted?
            sign_in(resource)
            logger.info resource.errors.full_messages
            session[:user_id] = resource.id
            render json: {success: true,email: resource.email},status:200
            return
        else
            invalid_signup_attempt(resource)
        end
    end

    def update_multipart
        if current_user.update(register_multipart_params)
            # sign_in(resource)
            # logger.info resource.errors.full_messages
            sign_in(current_user, :bypass=>true)
            session[:user_id] = current_user.id
            render json: {success: true,email: current_user.email},status:200
            return
        else
            invalid_signup_attempt(current_user)
        end
    end

    private

    # Never trust parameters from the scary internet, only allow the white list through.
    def register_params
        params.require(:user).permit(:avatar, :name, :email, :password, :date_of_birth, :facebook_token, :facebook_account, :gender, :child, :employment_situation,
        {:humor_ids => []},{:temperaments => []}, :religion, :rent, :affective_situation, :schooling ,:experience_id, :account_type_id)
    end

    def register_multipart_params
        params.require(:user).permit(:avatar, :name, :email, :password, :date_of_birth, :facebook_token, :facebook_account, :gender, :child, :employment_situation,
        {:humor_ids => []},{:temperaments => []}, :religion, :rent, :affective_situation, :schooling ,:experience_id, :account_type_id)
    end

    def invalid_signup_attempt(resource)
        logger.info resource.errors.full_messages
        warden.custom_failure!
        render json: resource.errors ,status: 401
    end

end