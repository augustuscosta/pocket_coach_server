class Api::WheelsController < Api::BaseController

	def index
		@wheels = Wheel.where(user: current_user)
		render 'wheels/index.json.jbuilder'
	end

	def create_wheel
	    @wheel = Wheel.new(wheel_params)
	    @wheel.user = current_user
		@wheel.save

		render 'wheels/show.json.jbuilder' 
		return @wheel
	end

	def show_wheel
		@wheel = Wheel.find(params[:id])
		render 'wheels/show.json.jbuilder' 
	end

	def wheel_params
		params.require(:wheel).permit(:emotional_balance, :health_and_disposition, :intellectual_development, :career_and_business, :finances, :social_contribution, :recreation, :satisfaction_and_fullfillment, :spirituality, :family, :relationship_with_love, :social_life, :user_ids)
	end


end 