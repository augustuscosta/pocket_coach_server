class Api::GoalsController < Api::BaseController

	before_filter :authenticate_user!
    respond_to :json
  
	def index
		@goals = current_user.goals
		render 'goals/index.json.jbuilder'
	end

	def show_goal
		@goal = Goal.find(params[:id])
		render 'goals/show.json.jbuilder' 
	end

	    # POST /resource
	def create_multipart_goal
	    
	    if params['goal']['date_realization'] >= DateTime.now
	    	@goal = Goal.new
		    @goal.user = current_user
		    @goal.description = params['goal']['description']
			@goal.status = "Em Andamento"
			@goal.image = params['goal']['image']
			@goal.date_realization = params['goal']['date_realization']
			@goal.specific = params['goal']['specific']
			@goal.temporal = params['goal']['temporal']
			@goal.relevant = params['goal']['relevant']
			@goal.reachable = params['goal']['reachable']
			@goal.measurable = params['goal']['measurable']
			@goal.ecological = params['goal']['ecological']
			@goal.save

			if @goal.persisted?
				render 'goals/show.json.jbuilder' 
			    return @goal
			else
				logger.info @goal.errors.full_messages
			end

		else
			render json: { error: "Data inferior a data atual.", status: 400 } , status: :bad_request
		end
	end

	def delete_goal
        @goal = Goal.find(params[:id])
        @goal.destroy
        render json: {success: true}
    end

    def finalize_goal
        @goal = Goal.find(params[:id])
        @goal.status = "Finalizada"
        @goal.save
        render json: {success: true}
    end

end