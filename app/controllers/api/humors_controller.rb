class Api::HumorsController < Api::BaseController

	respond_to :json

	def index
		@humors = Humor.where(user: current_user)
		render 'humors/index.json.jbuilder'
	end

	def create_humor
		@humor = Humor.new
		@humor.user = current_user
		@humor.name = params['humor']['name']
		@humor.date = DateTime.now
		@humor.save

		render 'humors/show.json.jbuilder' 
	end

end