class Api::ExperiencesController < Api::BaseController

  respond_to :json

  def index
    @experiences = Experience.all
    render 'experiences/index.json.jbuilder'
  end
  
end