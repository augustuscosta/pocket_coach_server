class Api::TasksController < Api::BaseController

    before_filter :authenticate_user!
    respond_to :json

    def index
        task_list = Array.new
        current_user.goals.each do |goal|
            goal.tasks.each do |task|
                task_list << task
            end
        end
        @tasks = task_list.sort_by {|task| task.date_realization }
        render 'tasks/index.json.jbuilder'
    end

    def get_tasks
        @tasks = Task.where(goal_id: params[:goal_id]).order(:date_realization)
        render 'tasks/index.json.jbuilder'
    end

    def show_task
        @task = Task.find(params[:id])
        render 'tasks/show.json.jbuilder' 
    end

    # Uma vez , Semanal, Mensal
    def create_task

        # logger.info "time 1"
       
        datahora = Time.at(params['task']['date_realization'] /1000).to_datetime.utc
        
        if  datahora + 3.hours >= DateTime.now
            @task = Task.new
            @task.goal_id = params['task']['goal_id']
            @task.description =  params['task']['description']
            @task.date_realization = datahora
            @task.status = params['task']['status']
            @task.type_frequency = params['task']['type_frequency']

            if @task.type_frequency == "Semanal"
                @week_days = params['week_days']
                Task.create_by_weekday(@task.goal_id, @task.description, @task.date_realization, @task.status, @task.type_frequency, @week_days )
                render json: {success: true}

            elsif @task.type_frequency == "Mensal"
                @day_of_month = params['day_of_month']
                Task.create_by_month(@task.goal_id, @task.description, @task.date_realization, @task.status, @task.type_frequency, @day_of_month)
                render json: {success: true}

            else
                @task.save
                render json: {success: true}
            end

            goal = Goal.find(@task.goal_id)
            calculate_status(goal)

        else
            render json: { error: "Data inferior a data atual.", status: 400 } , status: :bad_request
        end

    end

    def delete_task
        @task = Task.find(params[:id])
        @task.destroy

        goal = Goal.find(@task.goal_id)
        calculate_status(goal)
        render json: {success: true}
    end

    def finalize_task
        @task = Task.find(params[:id])
        @task.status = "Finalizada"
        @task.save

        goal = Goal.find(@task.goal_id)
        calculate_status(goal)
        
        render json: {success: true}
    end

    def calculate_status(goal)
        values = Array.new
        finalized = Array.new
        percentage = 0
        tasks = goal.tasks

        for task in tasks do

            if task.goal_id != nil 
                values.push(task.goal_id)
            end 

            if task.status == "Finalizada"
                finalized.push(task.goal_id)
            end 

        end

        if values.size != 0 and finalized.size != 0
            percentage = finalized.size.to_f/values.size.to_f * 100
            goal.calculate_status = percentage.to_int
        else
            goal.calculate_status = percentage
        end 

        logger.info "DEPOIS DO METODO"
        logger.info goal.calculate_status

        goal.save
    end

end