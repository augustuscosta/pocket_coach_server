class Users::RegistrationsController < Devise::RegistrationsController

 skip_before_action :require_no_authentication, only: [:new, :create]
   
      def update_resource(resource, params)
        resource.update_without_password(params)
      end

     def password_required?
        !persisted? || !password.nil? || !password_confirmation.nil?
      end


    def create
      super 
    end

     protected

    def sign_up(resource_name, resource)
       if current_user == nil 
        sign_in(resource_name, resource)
      end
    end

    def after_sign_up_path_for(resource)
      if current_user.account_type_id != 1
        root_path
      else  
        users_path
      end   
    end

    def get_religions
      return ['Católica', 'Evangélica', 'Protestante', 'Ateu', 'Espírita', 'Islamismo']
    end

private

  def sign_up_params
    params.require(:user).permit(:avatar, :date_of_birth, :email, :password, :name, :facebook_token, :facebook_account,:gender,:child, :affective_situation, :employment_situation,
        {:humor_ids => []},{:wheels => []},{:temperaments => []}, :religion, :rent ,:schooling,:experience_id)
  end
end