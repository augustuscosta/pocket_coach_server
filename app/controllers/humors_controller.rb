class HumorsController < ApplicationController
  before_action :set_humor, only: [:show, :edit, :update, :destroy]

  # GET /humors
  # GET /humors.json
  def index
    @humors = Humor.all
  end

  # GET /humors/1
  # GET /humors/1.json
  def show
  end

  # GET /humors/new
  def new
    @humor = Humor.new
  end

  # GET /humors/1/edit
  def edit
  end

  # POST /humors
  # POST /humors.json
  def create
    @humor = Humor.new(humor_params)

    respond_to do |format|
      if @humor.save
        format.html { redirect_to @humor, notice: 'Humor was successfully created.' }
        format.json { render :show, status: :created, location: @humor }
      else
        format.html { render :new }
        format.json { render json: @humor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /humors/1
  # PATCH/PUT /humors/1.json
  def update
    respond_to do |format|
      if @humor.update(humor_params)
        format.html { redirect_to @humor, notice: 'Humor was successfully updated.' }
        format.json { render :show, status: :ok, location: @humor }
      else
        format.html { render :edit }
        format.json { render json: @humor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /humors/1
  # DELETE /humors/1.json
  def destroy
    @humor.destroy
    respond_to do |format|
      format.html { redirect_to humors_url, notice: 'Humor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_humor
      @humor = Humor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def humor_params
      params.require(:humor).permit(:name)
    end
end
