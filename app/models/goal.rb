class Goal < ActiveRecord::Base
    belongs_to :user
    has_many :tasks

    has_attached_file :image, default_url: "images/target_estiloso.png",styles: { medium: "300x300>", thumb: "100x100>"}
    validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

    # has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/casa.jpg"
    # validates_attachment :image, content_type: { content_type: ["image/jpg", "image/gif", "image/png"] }

    def self.calculate_status
        values = Array.new
        finalized = Array.new
        percentage = 0

        for task in tasks do

            if task.goal_id != nil 
                values.push(task.goal_id)
            end	

            if task.status == "Finalizada"
                finalized.push(task.goal_id)
            end	

        end

        if values.size != 0 and finalized.size != 0
            percentage = finalized.size.to_f/values.size.to_f * 100
            return percentage.to_int
        else
            return percentage
        end 

    end

end



