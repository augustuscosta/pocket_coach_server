class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable


    has_attached_file :avatar, default_url: "normal/people.png"  , styles: { medium: "300x300>", thumb: "100x100>"}
    validates_attachment :avatar, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

    devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable,
           :omniauthable, :omniauth_providers => [:facebook]

         
    belongs_to :account_type
    belongs_to :experience
    has_many :goals
    has_many :device_id
    has_many :humors
    has_many :temperaments
    has_many :wheels


    def self.from_omniauth(auth)
        where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
            user.provider = auth.provider
            user.uid      = auth.uid
            user.name     = auth.info.name
            user.email    = auth.info.email
            user.facebook_token = auth.credentials.token
            if auth.info.image.present?
              avatar_url = process_uri(auth.info.image)
              user.update_attribute(:avatar, URI.parse(avatar_url))
            end
            user.save
        end
    end

    def self.new_with_session(params, session)
        if session["devise.user_attributes"]
            new(session["devise.user_attributes"], without_protrection: true) do |user|
                user.attributes = params
                user.valid?
            end
        else
            super
        end
    end

    def password_required?
        super && blank?
    end

    def update_with_password(params, *options)
        if password.blank?
            update_attributes(params, *options)
        else
            super
        end
    end

    def email_required?
        false
    end

    def email_changed?
      false
    end

end
