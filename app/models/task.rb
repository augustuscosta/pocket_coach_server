class Task < ActiveRecord::Base
	belongs_to :goal


	def self.create_by_month(goal_id, description, date_realization, status, type_frequency, day_of_month)
		today = Date.today

		while today <= date_realization do

			if today.day == day_of_month
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			today += 1.day
		end
	end


	# 0 sunday, 1 monday, 2 tuesday, 3 wednesday, 4 thursday, 5 friday, 6 saturday
	def self.create_by_weekday(goal_id, description, date_realization, status, type_frequency, week_days)
		today = Date.today

		while today <=  date_realization do 

			if today.wday == 0 && week_days.include?('0')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 1 && week_days.include?('1')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 2 && week_days.include?('2')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 3 && week_days.include?('3')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 4 && week_days.include?('4')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 5 && week_days.include?('5')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			if today.wday == 6 && week_days.include?('6')
				Task.create_task(goal_id, description, status, type_frequency, today)
			end

			today += 1.day
		end
	end


    def self.create_task(goal_id, description, status, type_frequency, today)
    	task = Task.new
    	task.goal_id = goal_id
		task.description = description
		task.status = status
		task.type_frequency = type_frequency
		task.date_realization = today
		task.save
	end


end
