json.extract! task, :id,:description, :date_realization, :status, :type_frequency,:goal_id, :created_at, :updated_at
json.url task_url(task, format: :json)