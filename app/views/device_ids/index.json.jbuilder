json.array!(@device_ids) do |device_id|
  json.extract! device_id, :id, :token, :os, :user_id
  json.url device_id_url(device_id, format: :json)
end
