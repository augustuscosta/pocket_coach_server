json.extract! humor, :id, :name, :date, :user_id
json.url humor_url(humor, format: :json)