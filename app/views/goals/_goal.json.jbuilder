json.extract! goal, :id, :date_realization, :image, :description, :status,:calculate_status, :specific, :temporal, :relevant, :reachable, :measurable, :user_id, :ecological, :created_at, :updated_at
        
json.tasks goal.tasks

json.url goal_url(goal, format: :json)