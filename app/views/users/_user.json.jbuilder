json.extract! user, :id, :avatar, :name, :email, :password, :date_of_birth, :facebook_token, :facebook_account, :gender, :child, :rent, :affective_situation, :employment_situation, :account_type_id, :schooling, :religion, :experience_id, :created_at, :updated_at

		json.humor user.humors

		json.temperaments user.temperaments

		json.goals user.goals


		if  user.experience_id != nil
			json.experience do |json|
				json.id user.experience.id
				json.name user.experience.name
			end
		end


json.url user_url(user, format: :json)