require 'test_helper'

class AffectiveSituationsControllerTest < ActionController::TestCase
  setup do
    @affective_situation = affective_situations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:affective_situations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create affective_situation" do
    assert_difference('AffectiveSituation.count') do
      post :create, affective_situation: { name: @affective_situation.name }
    end

    assert_redirected_to affective_situation_path(assigns(:affective_situation))
  end

  test "should show affective_situation" do
    get :show, id: @affective_situation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @affective_situation
    assert_response :success
  end

  test "should update affective_situation" do
    patch :update, id: @affective_situation, affective_situation: { name: @affective_situation.name }
    assert_redirected_to affective_situation_path(assigns(:affective_situation))
  end

  test "should destroy affective_situation" do
    assert_difference('AffectiveSituation.count', -1) do
      delete :destroy, id: @affective_situation
    end

    assert_redirected_to affective_situations_path
  end
end
