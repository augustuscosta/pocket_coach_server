require 'test_helper'

class SmarterizedsControllerTest < ActionController::TestCase
  setup do
    @smarterized = smarterizeds(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:smarterizeds)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create smarterized" do
    assert_difference('Smarterized.count') do
      post :create, smarterized: { desciption: @smarterized.desciption, name: @smarterized.name }
    end

    assert_redirected_to smarterized_path(assigns(:smarterized))
  end

  test "should show smarterized" do
    get :show, id: @smarterized
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @smarterized
    assert_response :success
  end

  test "should update smarterized" do
    patch :update, id: @smarterized, smarterized: { desciption: @smarterized.desciption, name: @smarterized.name }
    assert_redirected_to smarterized_path(assigns(:smarterized))
  end

  test "should destroy smarterized" do
    assert_difference('Smarterized.count', -1) do
      delete :destroy, id: @smarterized
    end

    assert_redirected_to smarterizeds_path
  end
end
