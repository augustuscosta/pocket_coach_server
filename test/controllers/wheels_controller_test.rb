require 'test_helper'

class WheelsControllerTest < ActionController::TestCase
  setup do
    @wheel = wheels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wheels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wheel" do
    assert_difference('Wheel.count') do
      post :create, wheel: { career_and_business: @wheel.career_and_business, emotional_balance: @wheel.emotional_balance, family: @wheel.family, finances: @wheel.finances, health_and_disposition: @wheel.health_and_disposition, intellectual_development: @wheel.intellectual_development, recreation: @wheel.recreation, relationship_with_love: @wheel.relationship_with_love, satisfaction_and_fullfillment: @wheel.satisfaction_and_fullfillment, socialLife: @wheel.socialLife, social_contribution: @wheel.social_contribution, spirituality: @wheel.spirituality }
    end

    assert_redirected_to wheel_path(assigns(:wheel))
  end

  test "should show wheel" do
    get :show, id: @wheel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wheel
    assert_response :success
  end

  test "should update wheel" do
    patch :update, id: @wheel, wheel: { career_and_business: @wheel.career_and_business, emotional_balance: @wheel.emotional_balance, family: @wheel.family, finances: @wheel.finances, health_and_disposition: @wheel.health_and_disposition, intellectual_development: @wheel.intellectual_development, recreation: @wheel.recreation, relationship_with_love: @wheel.relationship_with_love, satisfaction_and_fullfillment: @wheel.satisfaction_and_fullfillment, socialLife: @wheel.socialLife, social_contribution: @wheel.social_contribution, spirituality: @wheel.spirituality }
    assert_redirected_to wheel_path(assigns(:wheel))
  end

  test "should destroy wheel" do
    assert_difference('Wheel.count', -1) do
      delete :destroy, id: @wheel
    end

    assert_redirected_to wheels_path
  end
end
