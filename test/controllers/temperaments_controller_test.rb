require 'test_helper'

class TemperamentsControllerTest < ActionController::TestCase
  setup do
    @temperament = temperaments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:temperaments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create temperament" do
    assert_difference('Temperament.count') do
      post :create, temperament: { coping_result: @temperament.coping_result, fury_result: @temperament.fury_result, inhibition_result: @temperament.inhibition_result, sensibility_result: @temperament.sensibility_result, will_result: @temperament.will_result, wish_result: @temperament.wish_result }
    end

    assert_redirected_to temperament_path(assigns(:temperament))
  end

  test "should show temperament" do
    get :show, id: @temperament
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @temperament
    assert_response :success
  end

  test "should update temperament" do
    patch :update, id: @temperament, temperament: { coping_result: @temperament.coping_result, fury_result: @temperament.fury_result, inhibition_result: @temperament.inhibition_result, sensibility_result: @temperament.sensibility_result, will_result: @temperament.will_result, wish_result: @temperament.wish_result }
    assert_redirected_to temperament_path(assigns(:temperament))
  end

  test "should destroy temperament" do
    assert_difference('Temperament.count', -1) do
      delete :destroy, id: @temperament
    end

    assert_redirected_to temperaments_path
  end
end
