require 'test_helper'

class HumorsControllerTest < ActionController::TestCase
  setup do
    @humor = humors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:humors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create humor" do
    assert_difference('Humor.count') do
      post :create, humor: { date: @humor.date, name: @humor.name, total_graphic: @humor.total_graphic }
    end

    assert_redirected_to humor_path(assigns(:humor))
  end

  test "should show humor" do
    get :show, id: @humor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @humor
    assert_response :success
  end

  test "should update humor" do
    patch :update, id: @humor, humor: { date: @humor.date, name: @humor.name, total_graphic: @humor.total_graphic }
    assert_redirected_to humor_path(assigns(:humor))
  end

  test "should destroy humor" do
    assert_difference('Humor.count', -1) do
      delete :destroy, id: @humor
    end

    assert_redirected_to humors_path
  end
end
