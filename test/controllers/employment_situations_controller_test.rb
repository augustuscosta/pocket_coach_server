require 'test_helper'

class EmploymentSituationsControllerTest < ActionController::TestCase
  setup do
    @employment_situation = employment_situations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employment_situations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create employment_situation" do
    assert_difference('EmploymentSituation.count') do
      post :create, employment_situation: { name: @employment_situation.name }
    end

    assert_redirected_to employment_situation_path(assigns(:employment_situation))
  end

  test "should show employment_situation" do
    get :show, id: @employment_situation
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @employment_situation
    assert_response :success
  end

  test "should update employment_situation" do
    patch :update, id: @employment_situation, employment_situation: { name: @employment_situation.name }
    assert_redirected_to employment_situation_path(assigns(:employment_situation))
  end

  test "should destroy employment_situation" do
    assert_difference('EmploymentSituation.count', -1) do
      delete :destroy, id: @employment_situation
    end

    assert_redirected_to employment_situations_path
  end
end
