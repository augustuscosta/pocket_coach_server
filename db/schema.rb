# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170327204740) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "device_ids", force: :cascade do |t|
    t.string   "token"
    t.string   "os"
    t.string   "session_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "device_ids", ["user_id"], name: "index_device_ids_on_user_id", using: :btree

  create_table "experiences", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "goals", force: :cascade do |t|
    t.date     "date_realization"
    t.string   "description"
    t.string   "status"
    t.float    "calculate_status"
    t.boolean  "specific"
    t.boolean  "temporal"
    t.boolean  "relevant"
    t.boolean  "reachable"
    t.boolean  "measurable"
    t.boolean  "ecological"
    t.integer  "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "goals", ["user_id"], name: "index_goals_on_user_id", using: :btree

  create_table "humors", force: :cascade do |t|
    t.string   "name"
    t.datetime "date"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.datetime "date_realization"
    t.string   "description"
    t.string   "status"
    t.string   "type_frequency"
    t.integer  "goal_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "tasks", ["goal_id"], name: "index_tasks_on_goal_id", using: :btree

  create_table "temperaments", force: :cascade do |t|
    t.integer  "will_result"
    t.integer  "wish_result"
    t.integer  "fury_result"
    t.integer  "inhibition_result"
    t.integer  "sensibility_result"
    t.integer  "coping_result"
    t.integer  "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "users", force: :cascade do |t|
    t.date     "date_of_birth"
    t.string   "password"
    t.string   "name"
    t.string   "facebook_token"
    t.boolean  "facebook_account"
    t.string   "schooling"
    t.string   "child"
    t.string   "gender"
    t.string   "affective_situation"
    t.string   "rent"
    t.string   "employment_situation"
    t.string   "religion"
    t.integer  "account_type_id"
    t.integer  "experience_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["account_type_id"], name: "index_users_on_account_type_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["experience_id"], name: "index_users_on_experience_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "wheels", force: :cascade do |t|
    t.integer  "emotional_balance"
    t.integer  "health_and_disposition"
    t.integer  "intellectual_development"
    t.integer  "career_and_business"
    t.integer  "finances"
    t.integer  "social_contribution"
    t.integer  "recreation"
    t.integer  "satisfaction_and_fullfillment"
    t.integer  "spirituality"
    t.integer  "family"
    t.integer  "relationship_with_love"
    t.integer  "social_life"
    t.integer  "user_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_foreign_key "device_ids", "users"
  add_foreign_key "goals", "users"
  add_foreign_key "tasks", "goals"
  add_foreign_key "users", "account_types"
  add_foreign_key "users", "experiences"
end
