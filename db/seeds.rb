# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


    AccountType.create(name: 'usuário')
   	AccountType.create(name: 'administrador')
    
    Experience.create(name:'Zona de conforto')
    Experience.create(name:'Iniciante') # 1 objetivo
    Experience.create(name:'Intermediário') # 6 objetivos + 3 meses
    Experience.create(name:'Avançado') # 10 objetivos + 6 meses
    Experience.create(name:'Master') # 15 objetivos + 1 ano
    Experience.create(name:'Veterano') # 20 objetivos + 1,5 anos

    User.create(name:'Paulo Santos', password:"123456",email:"paulo@gmail.com", experience_id:'1')

    Goal.create(image:File.new("app/assets/images/casa.jpg"),date_realization:"15/09/2017",description:"Comprar uma casa",status:"Em Andamento",user_id:'1')
    Goal.create(date_realization:"15/09/2017",description:"Viagem para o Havaí",status:"Em Andamento",user_id:'1')

    Task.create(description: "Terminar", date_realization:"15/06/2017", status:"Em andamento", goal_id:'1')
    Task.create(description: "Levantar",date_realization:"15/05/2017", status:"Vencida", goal_id:'1')
    Task.create(description: "Treinar",date_realization:"16/05/2017", status:"Vencida", goal_id:'1')
    Task.create(description: "Pular",date_realization:"18/07/2017", status:"Em andamento", goal_id:'1')
    Task.create(description: "Caminhar",date_realization:"20/07/2017", status:"Finalizada", goal_id:'1')

    Wheel.create(emotional_balance: '4', health_and_disposition: '4', intellectual_development: '4', career_and_business: '4', finances: '4', social_contribution: '4', recreation: '4', satisfaction_and_fullfillment: '4', spirituality: '4', family: '4', relationship_with_love: '4', social_life: '4', user_id: '1')

