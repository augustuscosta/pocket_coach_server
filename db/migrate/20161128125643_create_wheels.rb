class CreateWheels < ActiveRecord::Migration
  def change
    create_table :wheels do |t|
      t.integer :emotional_balance
      t.integer :health_and_disposition
      t.integer :intellectual_development
      t.integer :career_and_business
      t.integer :finances
      t.integer :social_contribution
      t.integer :recreation
      t.integer :satisfaction_and_fullfillment
      t.integer :spirituality
      t.integer :family
      t.integer :relationship_with_love
      t.integer :social_life
      t.references :user

      t.timestamps null: false
    end
  end
end
