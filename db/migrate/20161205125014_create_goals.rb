class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.date :date_realization
      t.string :description
      t.string :status
      t.float :calculate_status
      t.boolean :specific
      t.boolean	:temporal
      t.boolean :relevant
      t.boolean :reachable
      t.boolean :measurable
      t.boolean :ecological
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
