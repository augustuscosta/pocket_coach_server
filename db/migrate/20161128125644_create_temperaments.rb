class CreateTemperaments < ActiveRecord::Migration
  def change
    create_table :temperaments do |t|
      t.integer :will_result
      t.integer :wish_result
      t.integer :fury_result
      t.integer :inhibition_result
      t.integer :sensibility_result
      t.integer :coping_result
      t.references :user

      t.timestamps null: false
    end
  end
end
