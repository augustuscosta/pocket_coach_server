class CreateHumors < ActiveRecord::Migration
  def change
    create_table :humors do |t|
      t.string :name
      t.datetime :date
      t.references :user

      t.timestamps null: false
    end
  end
end
