class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.date :date_of_birth
      t.string :password
      t.string :name
      t.string :facebook_token
      t.boolean :facebook_account
      t.string :schooling
      t.string :child
      t.string :gender
      t.string :affective_situation
      t.string :rent
      t.string :employment_situation
      t.string :religion
      t.references :account_type, index: true, foreign_key: true
      t.references :experience, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
