class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.datetime :date_realization
      t.string :description
      t.string :status
      t.string :type_frequency
      t.references :goal, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
